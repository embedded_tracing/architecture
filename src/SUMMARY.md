# Summary

[Welcome](./welcome.md)
- [Introduction & Goals](./01_introduction_and_goals.md)
- [Context & Scope](./03_context_and_scope.md)
- [Building Blocks View](./05_building_blocks_view.md)
- [Crosscutting concepts](./08_crosscutting_concepts/overview.md)
  - [Required interfaces realization](./08_crosscutting_concepts/required_interfaces_realization.md)
  - [Trace point format](./08_crosscutting_concepts/trace_point_format.md)
  - [Unique trace point ids](./08_crosscutting_concepts/unique_trace_point_ids.md)
- [Architecture Decisions](./09_architecture_decisions.md)
