# Context and Scope

* embedded_tracing ecosystem provides tracing client library, logger and time interface and utilities
* Embedded libraries but especially embedded applications can use these libraries to enable tracing
* Since logging is very hardware and application specific, usually a custom logger is provided by the embedded application
* Trace points created by embedded_tracing on a target are usually processed on an external computer
  * This external computer does use the analysis utilities provided by the embedded_tracing ecosystem

![Context and Scope of embedded_tracing](./assets/context.drawio.svg "Context and scope of embedded_tracing")
