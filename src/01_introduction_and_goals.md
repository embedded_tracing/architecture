# Introduction and goals

embedded_tracing is a small ecosystem of libraries and utilities for creating traces in embedded and bare-metal applications and libraries. It currently focusses on the Rust programming language with the Zig programming language.

Its core principles are:

* Simple
  * Focussed on the most relevant features
  * More focussed on one possible solution vs. complex configuration or generalizations
* Fast and safe
  * Trace points are created with as few operations as possible
* Extendable
  * Interfaces and possibilities to extend with own implementations
* Hardware independent
  * Due to its extendable nature, the required interfaces can be implemented for different hardware
* Explicit APIs
  * There is no focus on macros
* compile time optimization where possible
* Link time variance
  * Realization of interfaces are linked during compile time and not set during runtime

## Goals

* The quality goals of the embedded_tracing architecture are inspired by ISO 25010 Quality characteristics:
  * Performance Efficiency
  * Reliability
