# Required interfaces realization

* embedded_tracing (client library) needs to call two external interfaces: `logger` and `time`
* These cannot be implemented in embedded_tracing itself since these interfaces are very hardware and environment specific

![Required interfaces realization](../assets/required_interfaces_realization.drawio.svg "Required interfaces realization")

* The following sections describe possible solution to this problem

## Using the facade & singleton pattern combination as in log crate

* Description:
  * The `logger` and `time` interfaces are implemented as singletons and facade to the actual implementations
  * This means the implementation analog to the way this is done in the [log](https://crates.io/crates/log) crate, see the `LOGGER` singleton in this crate [here](https://github.com/rust-lang/log/blob/b7e8147271e8d58d2ae9a9a38368de60daae5914/src/lib.rs)
  * However the access to this `LOGGER` must be protected which is a not trivial in embedded systems
  * This means another interface besides `logger` and `time` is required which provides mutex-like functionality like disabling interrupts, critical areas or exclusive area since such a functionality can be very hardware specific (spin-locks are not working in embedded systems due to the way how interrupts work, see [Spinlocks Considered Harmful](https://matklad.github.io/2020/01/02/spinlocks-considered-harmful.html))
  * If implemented as well as a singleton and facade pattern, access to this singleton must also be protected (especially during initialization, e.g. `set_exclusive_area_provider`)
  * Finally this can result in a hen-and-egg problem: one needs an exclusive area to create the provider for exclusive areas

* Advantages:
  * Well-studied and working pattern combination (facade & singleton)
  * It works
  * Other solution will most likely use this pattern combination as well (but maybe behind an abstraction as implementation detail)

* Disadvantages:
  * Another interface is required
  * Hen-and-egg problem
  * Singletons need to be set during runtime (initialization phase) which limits possible compile time optimizations
  * Order of initialization of all three singletons is necessary:
    1. Exclusive area provider
    2. Time
    3. Logger
  * This may introduce runtime errors when not done in this order

## Using extern interfaces

* Description:
  * embedded_tracing (client library) defines the two extern interfaces it requires

    ```rust
    extern "Rust" {
      fn get_current_time() -> Timestamp;
      fn log_trace_point(trace_point: TracePoint);
    }
    ```

  * In order to provide these extern interfaces, an application needs to implement them (but can use other crates to do this). This is also necessary for tests when used in libraries

    ```rust
      #[no_mangle]
      fn get_current_time() -> Timestamp {
        some_time_crate::get_current_time()
      }
      #[no_mangle]
      fn log_trace_point(trace_point: TracePoint) {
        some_logger_crate::log_crate(trace_point)
      }
    }
    ```

  * `#[no_mangle]` is needed in order for the linker to find the symbol
  * The crates implementing these interfaces must not use `#[no_mangle]` since then the symbols are not unique

* Advantages:
  * Clear intent is "communicated": these interfaces must be realized from extern
* Disadvantages:
  * Access to these interfaces are unsafe
  * It's inconvenient for libraries since these interfaces must be implemented for tests as well

## Paths overrides

* Description:
  * Works as described in [The Cargo Book](https://doc.rust-lang.org/cargo/reference/overriding-dependencies.html#paths-overrides)
  * embedded_tracing (client library) uses `embedded_tracing_logger` and `embedded_tracing_time` which are noop implementations (both are in crates.io)
  * Custom `logger` and `time` implementations override the standard implementations with paths overrides

* Advantages:
  * Very simple and ergonomic solution

* Disadvantages:
  * Limitations regarding the dependency graphs:

    > Path overrides are more restricted than the [patch] section, however, in that they cannot change the structure of the dependency graph.

    * This means that an implementation of `logger` may not use another crate, which is especially problematic since it may need additional crates especially for creating exclusive areas (if necessary)

## extern crates and rust compiler flags

* Description:
  * Instead of adding the dependencies to Cargo.toml one could define these (i.e. `logger` and `time`) as extern crates
  * This means that `rustc` must link against these crates
  * However since these are not created it would be necessary to add them via the link folder or direct path flags of `rustc`
  * Here my speculation:
    * Via `build.rs` the `embedded_tracing` client library could set the necessary flag
    * Maybe a downstream dependency (e.g. an embedded application) needs to set it via an environment variables
    * For testing `embedded_tracing` could use `logger` and `time` as dev dependencies

* Advantages:
  * A clear intent is communicated from `embedded_tracing`
  * No `unsafe` necessary

* Disadvantages:
  * For non-test builds one must use an implementation of `logger` and `time` otherwise link time errors will occur which may be hard to understand
  * This may affect libraries and their dependencies when these are build outside of the test context

* Open items:
  * Currently it is unclear how this would exactly work regarding the linking
  * Additionally one must link a crate with a particular name which must be compiled to an `.rlib` with name `embedded_tracing_logger`, it is not clear how this would work

* Links:
  * [Linking Rust Crates](http://blog.pnkfx.org/blog/2022/05/12/linking-rust-crates/)

## Source replacement

* Description:
  * Use source replacement in the vendoring use case as described in [The Cargo Book](https://doc.rust-lang.org/cargo/reference/source-replacement.html)
  * embedded_tracing (client library) uses `embedded_tracing_logger` and `embedded_tracing_time` which are noop implementations (both are in crates.io)
  * Source replacement replaces these with local implementations

* Advantages:
  * Very simple and ergonomic solution
  * The term vendoring is pretty fitting

* Disadvantages:
  * Limitations regarding the dependency graphs, see [The Carog Book](https://doc.rust-lang.org/cargo/reference/source-replacement.html)

  > Cargo has a core assumption about source replacement that the source code is exactly the same from both sources. Note that this also means that a replacement source is not allowed to have crates which are not present in the original source.

## Patching

* Description:
  * Use patching as described in [The Cargo Book](https://doc.rust-lang.org/cargo/reference/overriding-dependencies.html#the-patch-section)
  * embedded_tracing (client library) uses `embedded_tracing_logger` and `embedded_tracing_time` which are noop implementations (both are in crates.io)
  * Custom `logger` and `time` implementations override the standard implementations with paths overrides
    * patch overrides usually need to be with `path` or `git`
    * A local crate which wraps a crate from crates.io does also enable the usage of crates from crates.io (albeit less convenient)
  * Works for these transitive dependencies (at least in the test implementation)
  * However it needs to be defined on the top, see quote from [The Cargo Book](https://doc.rust-lang.org/cargo/reference/overriding-dependencies.html#working-with-an-unpublished-minor-version)
    > Remember that [patch] is applicable transitively but can only be defined at the top level so we consumers of my-library have to repeat the [patch] section if necessary. Here, though, the new uuid crate applies to both our dependency on uuid and the my-library -> uuid dependency. The uuid crate will be resolved to one version for this entire crate graph, 1.0.1, and it'll be pulled from the git repository.
  * It is not necessary to publish `embedded_tracing_logger` and `embedded_tracing_time` to crates.io since git paths can be overridden too, see [The Cargo Book](https://doc.rust-lang.org/cargo/reference/overriding-dependencies.html#overriding-repository-url)
  * **Caution**: The crate which overrides must define a version larger than the overridden version. This is also necessary when using git paths, and the dependency must defined with a tag

* Advantages:
  * Solution that works with the possibility to change the dependency tree
    * I.e. that a custom logger could itself use some additional crates
  * Does not use unsafe
  * Relatively ergonomic
  * Library owners don't need to do anything, besides using embedded_tracing

* Disadvantages:
  * "Abuses" a feature to patch dependency, i.e. the intent is not that clear (as with extern interfaces)

## Conclusion

* [Patching](#patching) is used since it is the best fitting solution
