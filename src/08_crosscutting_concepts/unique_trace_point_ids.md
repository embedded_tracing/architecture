# Unique trace point ids

* In order to use embedded_tracing in libraries but also get unique ids of all trace points used in a complete application, there must be a possibility to override the ids defined in a library
* The following sections discuss solution options

## Extern interfaces

* Access only via unsafe

## Patching

* May not work when regarding publication in crates.io

## Generation via build.rs

* May require usage of 2 environment variables, one for the library and one for the application
* generation and handling of the environment variables is handled in another crate

## TODO

* Update building blocks view diagram
