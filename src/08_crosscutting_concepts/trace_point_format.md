# Trace point format

* Description of the trace point format as it can be send on the wire or stored in memory
* Reasoning behind this format

![Trace point format](../assets/trace_point_format.drawio.svg "Trace point format")

|Name|Size (bits)|Position|Description|
|:-|-:|-:|:-|
|Timestamp|40|63-24|The timestamp of the trace point|
|Trace point type|2|23-22|Type of trace point, i.e. span open, span close and event|
|Log level|2|21-20|Log level of the event, i.e. error, warn, debug, trace|
|Trace point ID|20|19-0|The unique ID of the trace point|

* The timestamp is a `u8` array in *big endian* order, i.e. with the most significant byte first
  * In this way the timestamps of each trace point can be easily compared by humans when seen in binary format
* The trace type is next which should also simplify the interpretation by humans if necessary

## Size

* The size of the trace point is deliberately chosen to be 64 bits or 8 bytes in order to fit into one CAN frame
* It also occupies not to much space

## Trace point IDs

* In order to uniquely identify a trace point and differentiate trace points from one another, an ID is needed
* The usage of only 16 bits, i.e. 65335 different values may be too small for larger applications
* 20 bits will result in more than 1 million possible IDs, which should be enough for even the largest applications
* Also an event and a span may share the same ID since they differ in type, i.e.
  * More than 1 million different spans are possible and
  * More than 1 million different events are possible
  * Additionally the log level may also help to identify different trace points

## Log level

* Usually the following log levels are known and used:
  * Fatal
  * Error
  * Warn
  * Debug
  * Info
  * Trace
* In order to be able to differentiate the level in not more than 2 bits, the levels are reduced to the bare minimum:
  * Fatal and error are combined: The thinking is, if a fatal error occurs, the application will most likely produce additional information in handling this fatal error, that an additional log level is not necessary
  * Warn and Debug are kept since they have such a distinct use
  * Info and trace are combined as well since embedded_tracing is a tracing library the necessity to use this level is obvious
* Spans do only use the trace level as for now

## Timestamp

* 40 bits in order to provide enough resolution and reduce the possibility of overflows as much as possible
* A small table to discuss different time resolution / counter frequency and the period to overflow when using a 40 bit timestamp value

|Resolution|Time to overflow|Comment|
|-:|-:|:-|
|10 ns|> 3h|Tick count of a micro controller with 100 MHz|
|1 mus|> 305h|1 micro second resolution|
|1 ns|> 18 min|1 nano second resolution, e.g. tick count of a 1 GHz microprocessor|

* Even if using nano second resolution provided by a 1 GHz micro processor, an overflow "only" occurs every 18 minutes
* Expectation is that traces are taken from specific and more short-lived scenarios, however it is not perfect
