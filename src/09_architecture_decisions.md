# Architecture decisions

## ADR-XXX: Arch Decision Template

* Context:
* Decision:
* Rationale:
* Consequence(s):
* Status:
  * Accepted (YYYY-MM-DD)
  * Superseded by ADR-XXX (YYYY-MM-DD)
  * Declined (YYYY-MM-DD)
  * Proposed (YYYY-MM-DD)

## ADR-002: Types in embedded_tracing_types provide the minimum required interfaces

* Context:
  * Each API can result in the final binary an bloat the binary larger then necessary
* Decision:
  * Types in embedded_tracing_types will only provide the necessary interfaces
* Rationale:
  * Reduce the final binary size without the necessity to rely on the compiler
  * If these interfaces are used by custom realizations of logger or time, then these APIs will part of the final binary and therefore may result in unnecessary code bloat
* Consequence(s):
  * In order to provide better access to these types there is a necessity for an additional crate that enables the interpretation of the types
* Status:
  * Accepted (2022-05-29)

## ADR-001: Use Rust Edition 2018

* Context:
  * It is possible to define the Rust edition for each crate.
* Decision:
  * All embedded_tracing crates for the embedded systems will use edition 2018.
* Rationale:
  * We expect that tool chains like ferrocene will focus on Rust Edition 2018 before any other.
* Consequence(s):
  * Some newer and powerful features of Rust cannot be used.
* Status:
  * Proposed (2022-05-29)
