# Building blocks view embedded_tracing

* embedded_tracing is the client library for libraries and applications for tracing
  * embedded_tracing (the client library) just provides the APIs to create trace points by creating spans or trace events
* embedded_tracing uses embedded_tracing_logger and embedded_tracing_time to log trace points and get timestamps
  * Both are interfaces and facades to be realized by custom loggers and timestamp providers
  * The logging and the creation of time stamps is very hardware and application specific, therefore an interface and a facade is required
  * Contrary to the way the facade is realized in [log](https://crates.io/crates/log) a logger and the time implementation is not set during runtime by defining the singletons in embedded_tracing itself but by overriding the interfaces during compile / link time
  * Although the realizations of logger and time may most likely involve the necessity of handling singletons, the way the access to this singleton is very much hardware and platform dependent (e.g. using embedded_tracing in environment with an OS and multithreading vs.bare metal and interrupts)
* An embedded application usually provides, instantiates and configures the logger and the time implementation
* Additionally it provides the embedded_tracing_ids library which is used by all libraries that use embedded_tracing for tracing
  * All IDs defined by these libraries are then overridden by this library
  * This enables an application to provide a unique ID for each trace point

![Building blocks](./assets/building_blocks.drawio.svg "Building blocks")

TODO: Diagram needs rework due to handling of embedded_tracing ids

## Relevant interfaces

### embedded_tracing_logger

```rust,ignore

// NOT Needed since this can be done or not by the logger
// Checks if log level is enabled.
// If false trace point is not logged.
// Enables loggers reduce the number of logged trace point
fn log_level_enabled(log_level:LogLevel) -> boolean {
  // ... 
}

// NOT Needed since this can be done or not by the logger
// Checks if trace id is enabled.
// If false trace point is not logged.
// Enables loggers to reduce the number of trace points by disabling specific trace IDs. 
//fn id_enabled(trace_id:TraceId) -> boolean {
  // ...
//}

// Logs trace points
fn log_trace_point(trace_point: TracePoint) {
  // ...
}

```

### embedded_tracing_time

```rust,ignore
// Returns the current time which is used in the trace point
fn get_current_time() -> Timestamp {
  // ...
}
```
